/*db.users.insertMany([
{
   "firstname": "Mary Jane",
   "lastname": "Watson",
   "email": "mjtiger@gmail.com",
   "password": "tigerjackpot15" ,
   "isAdmin": false
},
{
   "firstname": "Gwen",
   "lastname": "Stacy",
   "email": "stacyTech@gmail.com",
   "password": "stacyTech1991" ,
   "isAdmin": true
},
{
   "firstname": "Peter",
   "lastname": "Parker",
   "email": "peterWebDev@gmail.com",
   "password": "webdeveloperPeter" ,
   "isAdmin": true
},
{
   "firstname": "Otto",
   "lastname": "Octavius",
   "email": "ottoOctopi@gmail.com",
   "password": "docOck15" ,
   "isAdmin": true
},
{
   "firstname": "Jonah",
   "lastname": "Jameson",
   "email": "jjjameson@gmail.com",
   "password": "spideisamenace" ,
   "isAdmin": false
}
])*/
//$regex - this query operator wil allow us to find documents which will match the characters/pattern of the characters we are looking for
//db.users.find({"firstname": {$regex: 'o'}})
//db.users.find({"lastname": {$regex: 'O'}})

//$options - used to make regex case insensitive
//db.users.find("lastname": {$regex: 'O':, #options: '$i'}})
//You can also find for documents that matches a specific word
//db.users.find({"email": {$regex: 'web', $options: '$i'}})
//db.products.find({name: {$regex: 'phone', $options: '$i'}})

/*db.products.find({name: {$regex: 'razer', $options: '$i'}})
db.products.find({name: {$regex: 'rakk', $options: '$i'}})*/

//$or and $and
//$or operator - at least one of our condition is met
//db.products.find({$or: [{"name": {$regex: 'x', $options: '$i'}}, {"price": {$lte: 10000}}]})
//db.products.find({$or: [{"name": {$regex: 'x', $options: '$i'}}, {"price": 30000}]})
//db.users.find({$or: [{"firstname": {$regex: 'a', $options: '$i'}}, {"isAdmin": true}]})
//db.users.find({$or: [{"lastname": {$regex: 'w', $options: '$i'}}, {"isAdmin": false }]})


//$and operatr - both conditions are met
//db.products.find({$and: [{"name": {$regex: 'razer', $options: '$i'}}, {"price": {$gte: 3000}}]})
//db.users.find({$and: [{$"lastname": {$regex: 'w', $options: '$i'}}, {"isAdmin": false}]})
//db.users.find({$and: [{"lastname": {$regex: 'a', $options: '$i'}}, {"firstname": {$regex: 'e', $options: '$i'}}]})

//Field Projection
//db.users.find({}, {"_id":0, "password": 0})
//find() can have two arguments
//db.users.find({query}, {projection})
//Field Projection allows us to hide/show properties/fields of the returned documents after a query.
//db.products.find({"price": {$lte:3000}}, {"_id": 0, "isActive": 0})
//db.users.find({"isAdmin": false}, {"_id": 0, "email": 1})
//In field projection, 0 means hide and 1 means show.
//db.collection.find({query}, {"field": 0/1})
//db.users.find({"isAdmin": false}, {"_id": 0, "email": 1})
//_id has to be explicitly hidden if you want to show only 1 field.
//db.products.find({}, {"price": 1})
//db.products.find({"price": {%gte:10000}}, {"_id": 0, "name": 1, "price": 1})
